<?php
require_once ('../Modele/ModeleUtilisateur.php'); // chargement du modèle
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue('utilisateur/liste.php', ["utilisateurs" => $utilisateurs]);
        //require ('../vue/utilisateur/liste.php');  //"redirige" vers la vue
    }

    public static function afficherDetail() : void
    {
        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($_GET['login']);
        if(is_null($utilisateur)){
            self::afficherVue('utilisateur/erreur.php');
            //require ('../vue/utilisateur/erreur.php');
        }
        else {
            self::afficherVue('utilisateur/detail.php', [
                "utilisateur" => ModeleUtilisateur::recupererUtilisateurParLogin($_GET['login'])]);
            //require('../vue/utilisateur/detail.php');
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulaireCreation() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue('utilisateur/formulaireCreation.php', ["utilisateurs" => $utilisateurs]);
        //require ('../vue/utilisateur/liste.php');  //"redirige" vers la vue
    }

    public static function creerDepuisFormulaire() : void {
        $utilisateur = new ModeleUtilisateur($_GET['login'], $_GET['nom'], $_GET['prenom']);
        $utilisateur->ajouter();
        self::afficherListe();
        //require ('../vue/utilisateur/liste.php');  //"redirige" vers la vue
    }

}
?>
