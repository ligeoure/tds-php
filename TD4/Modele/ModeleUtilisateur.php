<?php

require_once 'ConnexionBaseDeDonnees.php';

class ModeleUtilisateur {


    private string $login;
    private string $nom;
    private string $prenom;
    /**
     * @var Trajet[]|null
     */
    private ?array $trajetsCommePassager;

    // un getter
    public function getNom() : string {
        return $this->nom;
    }

    public function getPrenom() : string {
        return $this->prenom;
    }

    public function getLogin() : string {
        return $this->login;
    }

    // un setter
    public function setNom(string $nom) {
        $this->nom = $nom;
    }

    public function setPrenom(string $prenom) {
        $this->prenom = $prenom;
    }

    public function setLogin(string $login) {
        $this->login = substr($login, 0, 64);
    }

    // un constructeur
    public function __construct(
      $login,
      $nom,
      $prenom,
   ) {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
//    public function __toString() {
//      $result = $this->getNom() . " " . $this->getPrenom() . " " . $this->getLogin() . " ";
//        if (!empty($this->trajetsCommePassager)) {
//            $result .= "<p>Trajets : <ul>";
//            foreach ($this->trajetsCommePassager as $trajets) {
//                $result .= "<li>$trajets</li>";
//            }
//            $result .= "</ul></p>";
//        } else {
//            $result .= "<p>Aucun trajet pour ce passager.</p>";
//        }
//
//        return $result;
//    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : ModeleUtilisateur
    {
       $utilisateur = new ModeleUtilisateur(
            $utilisateurFormatTableau['login'],
            $utilisateurFormatTableau['nom'],
            $utilisateurFormatTableau['prenom'],);

       return $utilisateur;
    }

    public static function recupererUtilisateurs() : array {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $pdoStatement = $pdo->query("SELECT * FROM utilisateur");
        $utilisateurs = [];

        while ($utilisateurFormatTableau = $pdoStatement->fetch(PDO::FETCH_ASSOC)) {
            $utilisateurs[] = ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }

        return $utilisateurs;
    }

    public function getTrajetsCommePassager(): ?array
    {
        if ($this->trajetsCommePassager === null) {
            $this->trajetsCommePassager = $this->recupererTrajetsCommePassager();
        }
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }

    public static function recupererUtilisateurParLogin(string $login) : ?ModeleUtilisateur {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();

        if($utilisateurFormatTableau==false){ return null; }


        return ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function ajouter() : void {
        $login= $this->getLogin();
        $nom = $this->getNom();
        $prenom = $this->getPrenom();
        $values = array(
            "loginTag" => $login,
            "nomTag" => $nom,
            "prenomTag" => $prenom,
        );
        $sql = "INSERT INTO utilisateur (login, nom, prenom) VALUES (:loginTag, :nomTag, :prenomTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $pdoStatement->execute($values);
    }

    /**
     * @return Trajet[]
     */
    private function recupererTrajetsCommePassager() : array {
        $Login = $this->getLogin();
        $trajets = [];
        $sql = "SELECT * FROM trajet JOIN passager ON trajet.id=passager.trajetId WHERE passagerLogin = :loginTag;
";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $Login,
        );

        $pdoStatement->execute($values);

        while ($trajetFormatTableau = $pdoStatement->fetch(PDO::FETCH_ASSOC)) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }



}
?>

