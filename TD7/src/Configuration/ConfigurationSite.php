<?php

namespace App\Covoiturage\Configuration;

class ConfigurationSite
{
    public static function getSessionExpiration(): int
    {
        return 1800;
    }
}