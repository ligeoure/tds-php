<?php

namespace  App\Covoiturage\Modele\DataObject;

use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Trajet;

class Utilisateur extends AbstractDataObject {


    private string $login;
    private string $nom;
    private string $prenom;
    /**
     * @var Trajet[]|null
     */
    private ?array $trajetsCommePassager;

    // un getter
    public function getNom() : string {
        return $this->nom;
    }

    public function getPrenom() : string {
        return $this->prenom;
    }

    public function getLogin() : string {
        return $this->login;
    }

    // un setter
    public function setNom(string $nom) {
        $this->nom = $nom;
    }

    public function setPrenom(string $prenom) {
        $this->prenom = $prenom;
    }

    public function setLogin(string $login) {
        $this->login = substr($login, 0, 64);
    }

    // un constructeur
    public function __construct(
      $login,
      $nom,
      $prenom,
   ) {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

//     Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString() {
      $result = $this->getNom() . " " . $this->getPrenom() . " " . $this->getLogin() . " ";
        if (!empty($this->trajetsCommePassager)) {
            $result .= "<p>Trajets : <ul>";
            foreach ($this->trajetsCommePassager as $trajets) {
                $result .= "<li>$trajets</li>";
            }
            $result .= "</ul></p>";
        } else {
            $result .= "<p>Aucun trajet pour ce passager.</p>";
        }

        return $result;
    }


    public function getTrajetsCommePassager(): ?array
    {
        if ($this->trajetsCommePassager === null) {
            $this->trajetsCommePassager = UtilisateurRepository::recupererTrajetsCommePassager($this);
        }
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }


}
?>

