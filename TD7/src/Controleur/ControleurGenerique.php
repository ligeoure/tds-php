<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique
{

    protected static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulairePreference() {
        self::afficherVue('vueGenerale.php', [
            "titre" => "Formulaire de préférences",
            "cheminCorpsVue" => "formulairePreference.php"
        ]);
    }

    protected static function enregistrerPreference()
    {
        $defaut = $_GET['controleur_defaut'];
        PreferenceControleur::enregistrer($defaut);
        self::afficherVue('vueGenerale.php', [
            "titre" => "Préférence enregistrée",
            "cheminCorpsVue" => "preferenceEnregistree.php"
        ]);
    }

    public static function afficherErreur(string $message): void
    {
        self::afficherVue('vueGenerale.php', [
            "titre" => "Erreur",
            "cheminCorpsVue" => "erreur.php",
            "messageErreur" => $message
        ]);
    }

}