<?php


namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use Exception;

class ControleurUtilisateur extends ControleurGenerique
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        /*self::deposerCookie();
        self::lireCookie();*/
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherDetail(): void
    {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        if (is_null($utilisateur)) {
            self::afficherErreur($_GET['login'] . " n'existe pas");
        } else {
            self::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "Detail de l'utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);  //"redirige" vers la vue

        }
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', [ "titre" => "Formulaire de création d'un Utilisateur", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);  //"redirige" vers la vue
    }

    public static function creerDepuisFormulaire(): void
    {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository)->ajouter($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["utilisateurs" => $utilisateurs,"titre" => "Creation d'un utilisateur", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
    }

    public static function afficherErreur(string $messageErreur = "") : void {
        self::afficherVue('vueGenerale.php', ["titre" => "Erreur", "messageErreur" => $messageErreur, "cheminCorpsVue" => "utilisateur/erreur.php"]);
    }

    public static function supprimer(): void
    {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        $login = $utilisateur->getLogin();
        if (is_null($utilisateur)) {
            self::afficherErreur($_GET['login'] . " n'existe pas");
        } else {
            (new UtilisateurRepository())->supprimerParLogin($_GET['login']);
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            self::afficherVue('vueGenerale.php', ["login" => $login,"titre" => "Utilisateur supprimé", "uti" => $utilisateur, "utilisateurs" => $utilisateurs, "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php"]);
        }
    }

    public static function afficherFormulaireMiseAJour()
    {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        if (is_null($utilisateur)) {
            self::afficherErreur($_GET['login'] . " n'existe pas");
        } else {
            self::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "FormulaireMiseAJour Utilisateur", "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php"]);
        }
    }

    public static function mettreAJour()
    {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        $login = $utilisateur->getLogin();
        if (is_null($utilisateur)) {
            self::afficherErreur();
        } else {
            (new UtilisateurRepository)->mettreAJour($utilisateur);
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            self::afficherVue('vueGenerale.php', ["login" => $login,"utilisateur" => $utilisateur, "utilisateurs" => $utilisateurs,"titre" => "Utilisateur MiseAJour", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php"]);
        }

    }

    /**
     * @return Utilisateur
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $utilisateur = new Utilisateur($tableauDonneesFormulaire['login'], $tableauDonneesFormulaire['nom'], $tableauDonneesFormulaire['prenom']);
        return $utilisateur;
    }

    /*public static function deposerCookie()
    {
        $utilisateur = new Utilisateur("Test", "Test", "Test");
        Cookie::enregistrer("TestCookie", $utilisateur, time() + 3600);
    }


    public static function lireCookie()
    {
        echo Cookie::lire("TestCookie");
    }*/

    public static function afficherFormulairePreference()
    {
        ControleurGenerique::afficherFormulairePreference();
    }

    public static function enregistrerPreference()
    {
        ControleurGenerique::enregistrerPreference();
    }

    /*public static function testerSession(): void
    {
        try {
            // Start the session
            $session = Session::getInstance();
            echo "Session started. Session ID: " . session_id() . "<br>";

            // Write different types of variables to the session
            $session->enregistrer("utilisateur", "Cathy Penneflamme");
            $session->enregistrer("tableau", ["clé1" => "valeur1", "clé2" => "valeur2"]);
            $session->enregistrer("objet", (object)["propriété1" => "valeur1", "propriété2" => "valeur2"]);

            // Read and display the session variables
            echo "Utilisateur: " . $session->lire("utilisateur") . "<br>";
            echo "Tableau: ";
            print_r($session->lire("tableau"));
            echo "<br>Objet: ";
            print_r($session->lire("objet"));
            echo "<br>";

            // Delete a session variable
            $session->supprimer("utilisateur");
            echo "Utilisateur après suppression: " . $session->lire("utilisateur") . "<br>";

            // Destroy the session
            $session->detruire();
            echo "Session destroyed. Session ID: " . session_id() . "<br>";
        } catch (Exception $e) {
            echo "Erreur: " . $e->getMessage();
        }
    }*/

}

?>