<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;

class ControleurTrajet extends ControleurGenerique
{

    public static function afficherListe(): void
    {
        $trajets = (new TrajetRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ['trajets' => $trajets, "titre" => "Liste des trajets", "cheminCorpsVue" => "trajet/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherErreur(string $messageErreur = "") : void {
        self::afficherVue('vueGenerale.php', ["titre" => "Erreur", "messageErreur" => $messageErreur, "cheminCorpsVue" => "trajet/erreur.php"]);
    }

    public static function afficherDetail(): void
    {
        $trajet = (new TrajetRepository())->recupererParClePrimaire($_GET['id']);
        if (is_null($trajet)) {
            self::afficherErreur($_GET['id'] . " n'existe pas");
        } else {
            self::afficherVue('vueGenerale.php', ["trajet" => $trajet, "titre" => "Detail de l'utilisateur", "cheminCorpsVue" => "trajet/detail.php"]);  //"redirige" vers la vue

        }
    }

    public static function supprimer(): void
    {
        $trajet = (new TrajetRepository())->recupererParClePrimaire($_GET['id']);
        if (is_null($trajet)) {
            self::afficherErreur($_GET['id'] . " n'existe pas");
        } else {
            (new TrajetRepository())->supprimerParLogin($_GET['id']);
            $trajets = (new TrajetRepository())->recuperer();
            self::afficherVue('vueGenerale.php', ["titre" => "Trajet supprimé", "trajet" => $trajet, "trajets" => $trajets, "cheminCorpsVue" => "trajet/trajetSupprime.php"]);
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', [ "titre" => "Formulaire de création d'un Trajet", "cheminCorpsVue" => "trajet/formulaireCreation.php"]);  //"redirige" vers la vue
    }

    public static function creerDepuisFormulaire() : void {

        $trajet = self::construireDepuisFormulaire($_GET);
        (new TrajetRepository())->ajouter($trajet);
        $trajets = (new TrajetRepository())->recuperer();
        self::afficherVue('../vue/vueGenerale.php', ["trajets" => $trajets, "titre" => "Liste des trajets", "cheminCorpsVue" => "trajet/trajetCree.php"]);
    }

    public static function afficherFormulaireMiseAJour()
    {
        $trajet = (new TrajetRepository())->recupererParClePrimaire($_GET['id']);
        if (is_null($trajet)) {
            self::afficherErreur($_GET['id'] . " n'existe pas");
        } else {
            self::afficherVue('vueGenerale.php', ["trajet" => $trajet, "titre" => "FormulaireMiseAJour Trajet", "cheminCorpsVue" => "trajet/formulaireMiseAJour.php"]);
        }
    }

    /**
     * @return Trajet
     * @throws \Exception
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
    {
        $id = $tableauDonneesFormulaire['id'] ?? null;
        $fumeur = isset($_GET['nonFumeur']) && $_GET['nonFumeur'] === 'on' ? 1 : 0;
        $dateString = $_GET['date'];
        $dateTime = new DateTime($dateString);
        $login = $_GET['conducteurLogin'];
        $trajet = new Trajet(
            null,
            $_GET['depart'],
            $_GET['arrivee'],
            $dateTime,
            $_GET['prix'],
            (new UtilisateurRepository())->recupererParClePrimaire($login),
            $fumeur
        );
        return $trajet;
    }

    public static function mettreAJour()
    {
        $trajet = self::construireDepuisFormulaire($_GET);
        if (is_null($trajet)) {
            self::afficherErreur();
        } else {
            (new TrajetRepository())->mettreAJour($trajet);
            $trajets = (new TrajetRepository())->recuperer();
            self::afficherVue('vueGenerale.php', ["trajet" => $trajet, "trajets" => $trajets,"titre" => "Trajet MiseAJour", "cheminCorpsVue" => "trajet/trajetMisAJour.php"]);
        }

    }

    public static function afficherFormulairePreference()
    {
        ControleurGenerique::afficherFormulairePreference();
    }

    public static function enregistrerPreference()
    {
        ControleurGenerique::enregistrerPreference();
    }

}