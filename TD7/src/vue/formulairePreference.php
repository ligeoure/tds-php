<?php

use App\Covoiturage\Lib\PreferenceControleur;

if (PreferenceControleur::lire()) {
    $preferenceControleur = PreferenceControleur::lire();
}
else {
    $preferenceControleur = "";
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Formulaire de Préférence</title>
</head>
<body>
<form method="get" action="controleurFrontal.php">
    <fieldset>
        <legend>Choisissez votre contrôleur par défaut :</legend>
        <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur" <?php echo ($preferenceControleur === 'utilisateur') ? 'checked' : ''; ?>>
        <label for="utilisateurId">Utilisateur</label>
        <br>
        <input type="radio" id="trajetId" name="controleur_defaut" value="trajet" <?php echo ($preferenceControleur === 'trajet') ? 'checked' : ''; ?>>
        <label for="trajetId">Trajet</label>
        <br>
        <input type="hidden" name="action" value="enregistrerPreference">
        <input type="submit" value="Enregistrer">
    </fieldset>
</form>
</body>
</html>