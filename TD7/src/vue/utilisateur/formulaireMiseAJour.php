<!DOCTYPE html>
<?php
/** @var ModeleUtilisateur[] $utilisateur */
$loginHTML = htmlspecialchars($utilisateur->getLogin());
$nomHTML = htmlspecialchars($utilisateur->getNom());
$prenomHTML = htmlspecialchars($utilisateur->getPrenom());
?>
<html>
<head>
    <meta charset="utf-8" />
    <title></title>
</head>

<body>

<form method="get" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" value="<?= $loginHTML?>" type="text" placeholder="leblancj" name="login" id="login_id" readonly/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom&#42;</label>
            <input class="InputAddOn-field" value="<?= $nomHTML?>" type="text" placeholder="LeBlanc" name="nom" id="nom_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prenom&#42;</label>
            <input class="InputAddOn-field" value="<?= $prenomHTML?>" type="text" placeholder="Juste" name="prenom" id="prenom_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="action_id">Action&#42;</label>
            <input class="InputAddOn-field" type='hidden' name='action' value='mettreAJour'>
        </p>
        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>

</body>
</html>