<?php

require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';
use App\Covoiturage\Controleur\ControleurUtilisateur;
//initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

// On récupère l'action passée dans l'URL
if(isset($_GET['action'])){
    if(isset($_GET['controleur'])){
        $controleur = $_GET['controleur'];
    } else {
        $controleur = 'utilisateur';
    }
    $nomDeClasseControleur = "App\Covoiturage\Controleur\Controleur" . ucfirst($controleur);
    $action = $_GET['action'];
    if(class_exists($nomDeClasseControleur)){
        if(in_array($_GET['action'],get_class_methods(ControleurUtilisateur::class))){
            $nomDeClasseControleur::$action();
        }
    }
    else{
        $nomDeClasseControleur::afficherErreur(" Pas d'action : $action");
    }
} else {
    ControleurUtilisateur::afficherListe();
}



?>
