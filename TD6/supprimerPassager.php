<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title> </title>
</head>

<body>




<?php

require_once __DIR__ . '/src/Modele/ConnexionBaseDeDonnees.php';
require_once 'Trajet.php';

$passagerLogin = $_GET['login'] ?? null;
$trajetId = $_GET['trajet_id'] ?? null;

$trajet = Trajet::recupererTrajetParId((int) $trajetId);

$success = $trajet->supprimerPassager($passagerLogin);

if ($success) {
    echo "Le passager $passagerLogin a été désinscrit du trajet avec succès.";
} else {
    echo "Erreur lors de la désinscription du passager $passagerLogin.";
}

?>

</body>
</html> 