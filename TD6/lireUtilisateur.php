<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title> </title>
</head>

<body>

<?php

require_once __DIR__ . '/src/Modele/ConnexionBaseDeDonnees.php';
require_once __DIR__ . '/src/Modele/Utilisateur.php';

//$pdo = ConnexionBaseDeDonnees::getPdo();
//$pdoStatement = $pdo->query("SELECT login, prenom, nom FROM utilisateur");
//$utilisateurFormatTableau = $pdoStatement->fetch();

//$Utilisateur  = new Utilisateur(
//        $utilisateurFormatTableau['login'],
//        $utilisateurFormatTableau['prenom'],
//        $utilisateurFormatTableau['nom']);

// echo $Utilisateur->__toString();


$utilisateurs = ModeleUtilisateur::getUtilisateurs();

foreach ($utilisateurs as $utilisateur) {
    echo "<h3>" . $utilisateur->getNom() . " " . $utilisateur->getPrenom() . " (" . $utilisateur->getLogin() . ")</h3>";

    $trajets = $utilisateur->getTrajetsCommePassager();
    if (!empty($trajets)) {
        echo "<p>Trajets en tant que passager :</p><ul>";
        foreach ($trajets as $trajet) {
            echo "<li>" . $trajet . "</li>";
        }
        echo "</ul>";
    } else {
        echo "<p>Aucun trajet pour cet utilisateur.</p>";
    }

}
?>

</body>
</html> 