<?php


namespace App\Covoiturage\Controleur;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurUtilisateur
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherDetail(): void
    {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        if (is_null($utilisateur)) {
            self::afficherErreur($_GET['login'] . " n'existe pas");
        } else {
            self::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "Detail de l'utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);  //"redirige" vers la vue

        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', [ "titre" => "Formulaire de création d'un Utilisateur", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);  //"redirige" vers la vue
    }

    public static function creerDepuisFormulaire(): void
    {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository)->ajouter($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["utilisateurs" => $utilisateurs,"titre" => "Creation d'un utilisateur", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
    }

    public static function afficherErreur(string $messageErreur = "") : void {
        self::afficherVue('vueGenerale.php', ["titre" => "Erreur", "messageErreur" => $messageErreur, "cheminCorpsVue" => "utilisateur/erreur.php"]);
    }

    public static function supprimer(): void
    {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        if (is_null($utilisateur)) {
            self::afficherErreur($_GET['login'] . " n'existe pas");
        } else {
            (new UtilisateurRepository())->supprimerParLogin($_GET['login']);
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            self::afficherVue('vueGenerale.php', ["titre" => "Utilisateur supprimé", "uti" => $utilisateur, "utilisateurs" => $utilisateurs, "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php"]);
        }
    }

    public static function afficherFormulaireMiseAJour()
    {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        if (is_null($utilisateur)) {
            self::afficherErreur($_GET['login'] . " n'existe pas");
        } else {
            self::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "FormulaireMiseAJour Utilisateur", "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php"]);
        }
    }

    public static function mettreAJour()
    {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        if (is_null($utilisateur)) {
            self::afficherErreur();
        } else {
            (new UtilisateurRepository)->mettreAJour($utilisateur);
            $utilisateurs = (new UtilisateurRepository())->recuperer();
            self::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "utilisateurs" => $utilisateurs,"titre" => "Utilisateur MiseAJour", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php"]);
        }

    }

    /**
     * @return Utilisateur
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $utilisateur = new Utilisateur($tableauDonneesFormulaire['login'], $tableauDonneesFormulaire['nom'], $tableauDonneesFormulaire['prenom']);
        return $utilisateur;
    }
}

?>