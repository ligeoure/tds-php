<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Création de Trajet</title>
</head>

<body>

<form method="get" action="controleurFrontal.php">
    <fieldset>
        <legend>Créer un Trajet :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="depart_id">Départ&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Paris" name="depart" id="depart_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="arrivee_id">Arrivée&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Lyon" name="arrivee" id="arrivee_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="date_id">Date&#42;</label>
            <input class="InputAddOn-field" type="date" name="date" id="date_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prix_id">Prix&#42;</label>
            <input class="InputAddOn-field" type="number" step="0.01" name="prix" id="prix_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="conducteurLogin_id">Login Conducteur&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="leblancj" name="conducteurLogin" id="conducteurLogin_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nonFumeur_id">Non Fumeur&#42;</label>
            <input class="InputAddOn-field" type="checkbox" name="nonFumeur" id="nonFumeur_id"/>
        </p>
        <input type="hidden" name="controleur" value="trajet">
        <input type="hidden" name="action" value="creerDepuisFormulaire">
        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>

</body>
</html>