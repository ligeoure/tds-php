<?php
use App\Covoiturage\Modele\DataObject\Trajet;
/** @var Trajet $trajet */

$nonFumeur = $trajet->isNonFumeur() ? " non fumeur" : "";
$result = "<p>Le trajet " . htmlspecialchars($nonFumeur) . " du {$trajet->getDate()->format('d/m/Y')} partira de " . htmlspecialchars($trajet->getDepart()) . " pour aller à " . htmlspecialchars($trajet->getArrivee()). " (conducteur: ". htmlspecialchars($trajet->getConducteur()->getPrenom()) . " " . htmlspecialchars($trajet->getConducteur()->getNom()).").</p>";

if (!empty($trajet->getPassagers())) {
    $result .= "<p>Passagers : <ul>";
    foreach ($trajet->getPassagers() as $passager) {
        $result .= "<li>".htmlspecialchars($passager->getPrenom()) . " " . htmlspecialchars($passager->getNom())."</li>";
        $result .= "<a href='supprimerPassager.php?login=" . htmlspecialchars($passager->getLogin()) . "&trajet_id=" . $this->getId() . "'>Désinscrire</a>";
    }
    $result .= "</ul></p>";
} else {
    $result .= "<p>Aucun passager pour ce trajet.</p>";
}

echo $result;
?>