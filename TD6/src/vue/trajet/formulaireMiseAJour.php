<?php
/** @var Trajet $trajet */

$fumeurChecked = $trajet->isNonFumeur() ? 'checked' : '';
?>
<!DOCTYPE html>
<html><head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title> Mon premier php </title>
</head>
<body>
<div>
    <form method="get" action="controleurFrontal.php">
        <fieldset>
            <legend>Mon formulaire :</legend>
            <input type="hidden" value="<?=htmlspecialchars($trajet->getId());?>">
            <p class="InputAddOn">
                <label class="InputAddOn-item" for="depart_id">Depart</label>
                <input class="InputAddOn-field" type="text" placeholder="Montpellier" value="<?=htmlspecialchars($trajet->getDepart());?>" name="depart" id="depart_id" required="">
            </p>
            <p class="InputAddOn">
                <label class="InputAddOn-item" for="arrivee_id">Arrivée</label>
                <input class="InputAddOn-field" type="text" placeholder="Sète" value="<?=htmlspecialchars($trajet->getArrivee());?>" name="arrivee" id="arrivee_id" required="">
            </p>
            <p class="InputAddOn">
                <label class="InputAddOn-item" for="date_id">Date</label>
                <input class="InputAddOn-field" type="date" placeholder="JJ/MM/AAAA" value="<?=htmlspecialchars( $trajet->getDate()->format("Y-m-d"));?>" name="date" id="date_id" required="">
            </p>
            <p class="InputAddOn">
                <label class="InputAddOn-item" for="prix_id">Prix</label>
                <input class="InputAddOn-field" type="number" placeholder="20" value="<?=htmlspecialchars($trajet->getPrix());?>" name="prix" id="prix_id" required="">
            </p>
            <p class="InputAddOn">
                <label class="InputAddOn-item" for="conducteurLogin_id">Login du conducteur</label>
                <input class="InputAddOn-field" type="text" placeholder="leblancj" value="<?=htmlspecialchars($trajet->getConducteur()->getLogin());?>" name="conducteurLogin" id="conducteurLogin_id" required="">
            </p>
            <p class="InputAddOn">
                <label class="InputAddOn-item" for="nonFumeur_id">Non Fumeur ?</label>
                <input class="InputAddOn-field" type="checkbox" name="nonFumeur" id="nonFumeur_id" <?=$fumeurChecked?>>
            </p>
            <p class="InputAddOn">
                <input class="InputAddOn-item" type='hidden' name='action' value='mettreAJour'>
                <input class="InputAddOn-item" type="hidden" name="controleur" value="trajet">
                <input class="InputAddOn-field" type="submit" value="Envoyer">
            </p>
        </fieldset>
    </form>
</div>
</body></html>
