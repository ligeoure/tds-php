<?php

require_once __DIR__ . '/src/Modele/ConnexionBaseDeDonnees.php';
require_once __DIR__ . '/src/Modele/ModeleUtilisateur.php';

class Trajet {

    private ?int $id;
    private string $depart;
    private string $arrivee;
    private DateTime $date;
    private int $prix;
    private ModeleUtilisateur $conducteur;
    private bool $nonFumeur;
    /**
     * @var ModeleUtilisateur[]
     */
    private array $passagers;

    public function __construct(
        ?int              $id,
        string            $depart,
        string            $arrivee,
        DateTime          $date,
        int               $prix,
        ModeleUtilisateur $conducteur,
        bool              $nonFumeur,
        array             $passagers
    )
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->prix = $prix;
        $this->conducteur = $conducteur;
        $this->nonFumeur = $nonFumeur;
        $this->passagers = $passagers;
    }

    public static function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]), // À changer
            $trajetTableau["prix"],
            ModeleUtilisateur::recupererUtilisateurParLogin($trajetTableau["conducteurLogin"]), // À changer
            $trajetTableau["nonFumeur"], // À changer ?
            []
        );
        $passagers = $trajet->recupererPassagers();
        $trajet->setPassagers($passagers);

        return $trajet;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDepart(): string
    {
        return $this->depart;
    }

    public function setDepart(string $depart): void
    {
        $this->depart = $depart;
    }

    public function getArrivee(): string
    {
        return $this->arrivee;
    }

    public function setArrivee(string $arrivee): void
    {
        $this->arrivee = $arrivee;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getPrix(): int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): void
    {
        $this->prix = $prix;
    }

    public function getConducteur(): ModeleUtilisateur
    {
        return $this->conducteur;
    }

    public function setConducteur(ModeleUtilisateur $conducteur): void
    {
        $this->conducteur = $conducteur;
    }

    public function isNonFumeur(): bool
    {
        return $this->nonFumeur;
    }

    public function setNonFumeur(bool $nonFumeur): void
    {
        $this->nonFumeur = $nonFumeur;
    }

    public function getPassagers(): array
    {
        return $this->passagers;
    }

    public function setPassagers(array $passagers): void
    {
        $this->passagers = $passagers;
    }

    public function __toString()
    {
        $nonFumeur = $this->nonFumeur ? " non fumeur" : "";
        $result = "<p>Le trajet{$nonFumeur} du {$this->date->format('d/m/Y')} partira de {$this->depart} pour aller à {$this->arrivee} (conducteur: {$this->conducteur->getPrenom()} {$this->conducteur->getNom()}).</p>";

        if (!empty($this->passagers)) {
            $result .= "<p>Passagers : <ul>";
            foreach ($this->passagers as $passager) {
                $result .= "<li>{$passager->getPrenom()} {$passager->getNom()}</li>";
                $result .= "<a href='supprimerPassager.php?login=" . $passager->getLogin() . "&trajet_id=" . $this->getId() . "'>Désinscrire</a>";
            }
            $result .= "</ul></p>";
        } else {
            $result .= "<p>Aucun passager pour ce trajet.</p>";
        }

        return $result;
    }

    /**
     * @return Trajet[]
     */
    public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }

    public function ajouter() : void {
        $login = $this->getConducteur()->getLogin();
        $depart= $this->getDepart();
        $arrivee = $this->getArrivee();
        $date = $this->getDate();
        $prix = $this->getPrix();
        $nonFumeur = $this->isNonFumeur() ? 1 : 0;
        $values = array(
            "loginTag" => $login,
            "departTag" => $depart,
            "arriveeTag" => $arrivee,
            "dateTag" => $date->format("Y-m-d"),
            "prixTag" => $prix,
            "nonFumeurTag" => $nonFumeur,
        );
        $sql = "INSERT INTO trajet (id, depart, arrivee, date, prix, conducteurLogin, nonFumeur) VALUES (null, :departTag, :arriveeTag, :dateTag, :prixTag, :loginTag, :nonFumeurTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $pdoStatement->execute($values);
    }

    /**
     * @return ModeleUtilisateur[]
     */
    private function recupererPassagers() : array {

        $idTrajet = $this->getId();
        $passagers = [];
        $sql = "SELECT * FROM `utilisateur` INNER join passager on utilisateur.login=passager.passagerLogin WHERE passager.trajetId=:idTag";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "idTag" => $idTrajet,
        );

        $pdoStatement->execute($values);

        $utilisateurFormatTableau = $pdoStatement->fetch();

        while ($utilisateurFormatTableau = $pdoStatement->fetch(PDO::FETCH_ASSOC)) {
            $passagers[] = ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }

        return $passagers;
    }

    public function supprimerPassager(string $passagerLogin): bool{
        $sql = "DELETE FROM passager WHERE passagerLogin = :passagerLogin AND trajetId = :trajetId";

        $pdo = ConnexionBaseDeDonnees::getPdo();
        $pdoStatement = $pdo->prepare($sql);

        $values = [
            "passagerLogin" => $passagerLogin,
            "trajetId" => $this->getId(),
        ];

        return $pdoStatement->execute($values);
    }

    public static function recupererTrajetParId(string $id) : ?Trajet {
        $sql = "SELECT * from trajet WHERE id = :idTrajetTag";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "idTrajetTag" => $id,
        );
        
        $pdoStatement->execute($values);
        
        $trajetFormatTableau = $pdoStatement->fetch();

        if($trajetFormatTableau==false){ return null; }


        return Trajet::construireDepuisTableauSQL($trajetFormatTableau);
    }

}
