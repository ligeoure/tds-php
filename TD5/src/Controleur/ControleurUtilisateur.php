<?php


namespace  App\Covoiturage\Controleur;
use App\Covoiturage\Configuration\ConfigurationBaseDeDonnees;

use  App\Covoiturage\Modele\ModeleUtilisateur;
class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue('../vue/vueGenerale.php', ["utilisateurs" => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);
        //require ('../vue/utilisateur/liste.php');  //"redirige" vers la vue
    }

    public static function afficherDetail() : void
    {
        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($_GET['login']);
        if(is_null($utilisateur)){
            self::afficherVue('../vue/utilisateur/erreur.php',["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);
            //require ('../vue/utilisateur/erreur.php');
        }
        else {
            self::afficherVue('../vue/utilisateur/detail.php', [
                "utilisateur" => ModeleUtilisateur::recupererUtilisateurParLogin($_GET['login'])]);
            //require('../vue/utilisateur/detail.php');
        }
    }

    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulaireCreation() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); //appel au modèle pour gérer la BD
        self::afficherVue('../vue/utilisateur/formulaireCreation.php', ["utilisateurs" => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);
        //require ('../vue/utilisateur/liste.php');  //"redirige" vers la vue
    }

    public static function creerDepuisFormulaire() : void {
        $utilisateur = new ModeleUtilisateur($_GET['login'], $_GET['nom'], $_GET['prenom']);
        $utilisateur->ajouter();
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
        self::afficherVue('../vue/vueGenerale.php', ["utilisateurs" => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
        //require ('../vue/utilisateur/liste.php');  //"redirige" vers la vue
    }

}
?>
