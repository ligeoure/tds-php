<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title> </title>
</head>

<body>

<?php

require_once 'ConnexionBaseDeDonnees.php';
require_once 'Utilisateur.php';

$pdo = ConnexionBaseDeDonnees::getPdo();
$pdoStatement = $pdo->query("SELECT login, prenom, nom FROM utilisateur");
$utilisateurFormatTableau = $pdoStatement->fetch();

$Utilisateur  = new Utilisateur(
        $utilisateurFormatTableau['login'],
        $utilisateurFormatTableau['prenom'],
        $utilisateurFormatTableau['nom']);

// echo $Utilisateur->__toString();
$utilisateurs = $Utilisateur::getUtilisateurs();
foreach ($utilisateurs as $utilisateur) {
    echo $utilisateur;
}
?>

</body>
</html> 