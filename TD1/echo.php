<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title> Mon premier php </title>
</head>

<body>
Voici le résultat du script PHP :
<?php
// Ceci est un commentaire PHP sur une ligne
/* Ceci est le 2ème type de commentaire PHP
sur plusieurs lignes */

// On met la chaine de caractères "hello" dans la variable 'texte'
// Les noms de variable commencent par $ en PHP
//$texte = "hello world !";

// On écrit le contenu de la variable 'texte' dans la page Web
//echo $texte;
//$prenom = "Marc";

// echo "Bonjour\n " . $prenom;
// echo "Bonjour\n $prenom";
// echo 'Bonjour\n $prenom';

// echo $prenom;
// echo "$prenom";

// $nom = 'Ligeour';
// $prenom = 'Enzo';
// $login = 'ligeoure';

// echo "Utilisateur $prenom $nom de login $login";

$utilisateur = ["prenom" => "Juste","nom"=>"Leblanc","login"=>"leblancj"];
$utilisateur1 = ["prenom" => "Just1","nom"=>"Leblanc1","login"=>"leblancj1"];

//echo "Utilisateur $utilisateur[prenom] $utilisateur[nom] de login $utilisateur[login]";

$utilisateurs =[1=>$utilisateur,2=>$utilisateur1];

//echo var_dump( $utilisateurs );

if(empty($utilisateurs)) { echo "Il n’y a aucun utilisateur."; }

else{
    echo "<ul>";
    foreach ($utilisateurs as $cle => $valeur){
        echo"<li>$valeur[prenom] $valeur[nom] de login $valeur[login] </li>";
    }
    echo "</ul>";
}


?>
</body>
</html> 