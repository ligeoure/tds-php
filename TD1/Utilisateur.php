<?php
class Utilisateur {
   
    private string $login;
    private string $nom;
    private string $prenom;
   
    // un getter      
    public function getNom() {
        return $this->nom;
    }

    public function getPrenom() {
        return $this->prenom;
    }

    public function getLogin() {
        return $this->login;
    }
   
    // un setter 
    public function setNom(string $nom) {
        $this->nom = $nom;
    }

    public function setPrenom(string $prenom) {
        $this->prenom = $prenom;
    }

    public function setLogin(string $login) {
        $this->login = substr($login, 0, 64);
    }
   
    // un constructeur
    public function __construct(
      $login,
      $nom, 
      $prenom,
   ) {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
    } 
              
    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString() {
      return $this->getNom() . " " . $this->getPrenom() . " " . $this->getLogin();
    }
}
?>

