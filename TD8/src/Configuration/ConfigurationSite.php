<?php

namespace App\Covoiturage\Configuration;

class ConfigurationSite
{
    public static function getSessionExpiration(): int
    {
        return 1800;
    }

    public static function getURLAbsolue(): string
    {
        return "http://localhost/tds-php/TD8/web/controleurFrontal.php";
    }

    public static function getDebug() : bool
    {
        return true;
    }

}