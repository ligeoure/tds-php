<?php

namespace App\Covoiturage\Modele\HTTP;

class Cookie
{
    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void
    {
        $val = serialize($valeur);

        if ($dureeExpiration == null) {
            $dureeExpiration = 0;
        }

        setcookie($cle, $val, $dureeExpiration);
    }

    public static function lire(string $cle): mixed
    {
        if (!isset($_COOKIE[$cle])) {
            return null;
        }

        return unserialize($_COOKIE[$cle]);
    }

    public static function contient($cle): bool
    {
        return isset($_COOKIE[$cle]);
    }

    public static function supprimer($cle): void
    {
        unset($_COOKIE[$cle]);
        setcookie($cle, "", 1);
    }
}