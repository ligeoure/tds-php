<?php

namespace  App\Covoiturage\Modele\DataObject;

use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\DataObject\Trajet;

class Utilisateur extends AbstractDataObject {


    private string $login;
    private string $nom;
    private string $prenom;
    private string $mdpHache;
    private bool $estAdmin;
    private string $email;

    private string $emailAValider;

    private string $nonce;

    /**
     * @var Trajet[]|null
     */
    private ?array $trajetsCommePassager;

    // un getter
    public function getNom() : string {
        return $this->nom;
    }

    public function getPrenom() : string {
        return $this->prenom;
    }

    public function getLogin() : string {
        return $this->login;
    }

    // un setter
    public function setNom(string $nom) {
        $this->nom = $nom;
    }

    public function setPrenom(string $prenom) {
        $this->prenom = $prenom;
    }

    public function setLogin(string $login) {
        $this->login = substr($login, 0, 64);
    }

    public function getMdpHache() : string {
        return $this->mdpHache;
    }

    public function setMdpHache(string $mdpHache) {
        $this->mdpHache = $mdpHache;
    }

    public function getEstAdmin() : bool {
        return $this->estAdmin;
    }

    public function setEstAdmin(bool $estAdmin) {
        $this->estAdmin = $estAdmin;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getEmailAValider(): string
    {
        return $this->emailAValider;
    }

    public function setEmailAValider(string $emailAValider): void
    {
        $this->emailAValider = $emailAValider;
    }

    public function getNonce(): string
    {
        return $this->nonce;
    }

    public function setNonce(string $nonce): void
    {
        $this->nonce = $nonce;
    }


    // un constructeur
    public function __construct(
      $login,
      $nom,
      $prenom,
      $mdpHache,
      $estAdmin = false,
      $email = '',
      $emailAValider = '',
      $nonce = ''
   ) {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->mdpHache = $mdpHache;
        $this->estAdmin = $estAdmin;
        $this->email = $email;
        $this->emailAValider = $emailAValider;
        $this->nonce = $nonce;
    }

//     Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString() {
      $result = $this->getNom() . " " . $this->getPrenom() . " " . $this->getLogin() . " ";
        if (!empty($this->trajetsCommePassager)) {
            $result .= "<p>Trajets : <ul>";
            foreach ($this->trajetsCommePassager as $trajets) {
                $result .= "<li>$trajets</li>";
            }
            $result .= "</ul></p>";
        } else {
            $result .= "<p>Aucun trajet pour ce passager.</p>";
        }

        return $result;
    }


    public function getTrajetsCommePassager(): ?array
    {
        if ($this->trajetsCommePassager === null) {
            $this->trajetsCommePassager = UtilisateurRepository::recupererTrajetsCommePassager($this);
        }
        return $this->trajetsCommePassager;
    }

    public function setTrajetsCommePassager(?array $trajetsCommePassager): void
    {
        $this->trajetsCommePassager = $trajetsCommePassager;
    }


}
?>

