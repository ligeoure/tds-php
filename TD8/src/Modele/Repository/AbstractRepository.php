<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;

abstract class AbstractRepository
{

    public function mettreAJour(AbstractDataObject $objet)
    {
        $values = $this->formatTableauSQL($objet);

        $setClause = join(", ", array_map(fn($colonne) => "$colonne = :{$colonne}Tag", $this->getNomsColonnes()));
        $sql = "UPDATE " . $this->getNomTable() . " SET ". $setClause . " WHERE " . $this->getNomClePrimaire() . " = :{$this->getNomClePrimaire()}Tag;";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $pdoStatement->execute($values);
    }

    public function ajouter(AbstractDataObject $objet): bool
    {

        $placeholders = join(", ", array_map(fn($colonne) => ":{$colonne}Tag", $this->getNomsColonnes()));
        $values = $this->formatTableauSQL($objet);

        $sql = "INSERT INTO ". $this->getNomTable() . " (". join(",", $this->getNomsColonnes()) .") VALUES($placeholders)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        return $pdoStatement->execute($values);
    }

    public function supprimerParLogin(string $valeurClePrimaire): void
    {
        $values = array(
            "valeurClePrimaireTag" => $valeurClePrimaire
        );
        $sql = "DELETE FROM " .  $this->getNomTable(). " WHERE ". $this->getNomClePrimaire() ." = :valeurClePrimaireTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $pdoStatement->execute($values);
    }

    public function recupererParClePrimaire(string $cle): ?AbstractDataObject
    {
        $nomtable = $this->getNomTable();
        $nomClePrimaire = $this->getNomClePrimaire();
        $sql = "SELECT * from $nomtable WHERE $nomClePrimaire = :cleTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "cleTag" => $cle,
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $objetFormatTableau = $pdoStatement->fetch();

        if (!$objetFormatTableau) {
            return null;
        }


        return $this->construireDepuisTableauSQL($objetFormatTableau);
    }

    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau) : AbstractDataObject;
    protected abstract function getNomTable() : string;

    protected abstract function getNomClePrimaire();

    public function recuperer(): array
    {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $requete = "SELECT * FROM " . $this->getNomTable();
        $pdoStatement = $pdo->query($requete);

        $tableau = [];
        foreach ($pdoStatement as $objetFormatTableau) {
            $tableau[] = $this->construireDepuisTableauSQL($objetFormatTableau);
        }
        return $tableau;
    }

    protected abstract function getNomsColonnes(): array;


}