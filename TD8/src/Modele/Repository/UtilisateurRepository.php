<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\Repository;
use App\Covoiturage\Modele\DataObject\Utilisateur;

class UtilisateurRepository extends AbstractRepository
{

    protected function formatTableauSQL(AbstractDataObject $utilisateur): array
    {
        /** @var Utilisateur $utilisateur */
        return array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom(),
            "mdpHacheTag" => $utilisateur->getMdpHache(),
            "estAdminTag" => $utilisateur->getEstAdmin() ? 1 : 0,
            "emailTag" => $utilisateur->getEmail(),
            "emailAValiderTag" => $utilisateur->getEmailAValider(),
            "nonceTag" => $utilisateur->getNonce(),
        );
    }


    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["login", "nom", "prenom", "mdpHache", "estAdmin", "email", "emailAValider", "nonce"];
    }

    protected function construireDepuisTableauSQL(array $objetFormatTableau) : Utilisateur
    {
        $utilisateur = new Utilisateur(
            $objetFormatTableau['login'],
            $objetFormatTableau['nom'],
            $objetFormatTableau['prenom'],
            $objetFormatTableau['mdpHache'],
            $objetFormatTableau['estAdmin'],
            $objetFormatTableau['email'],
            $objetFormatTableau['emailAValider'],
            $objetFormatTableau['nonce'],
        );

        return $utilisateur;
    }

    public static function recupererTrajetsCommePassager(Utilisateur $utilisateur) : array {
        $Login = $utilisateur->getLogin();
        $trajets = [];
        $sql = "SELECT * FROM trajet JOIN passager ON trajet.id=passager.trajetId WHERE passagerLogin = :loginTag;";

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $Login,
        );

        $pdoStatement->execute($values);

        foreach( $pdoStatement as $objetFormatTableau) {
            $trajets[] = (new TrajetRepository())->construireDepuisTableauSQL($objetFormatTableau);
        }

        return $trajets;
    }

    public function getNomTable(): string
    {
    return 'utilisateur';
    }

    public function getNomClePrimaire(): string
    {
    return 'login';
    }

}
?>