<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\Trajet;
use DateTime;

class TrajetRepository extends AbstractRepository
{

    /*public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = (new TrajetRepository)->construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }*/

    public static function recupererPassagers(Trajet $trajet) : array {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $id = $trajet->getId();
        $requete = "SELECT login, nom, prenom FROM utilisateur JOIN passager ON passager.passagerLogin = utilisateur.login WHERE passager.trajetId = $id";
        $pdoStatement = $pdo->query($requete);

        $tableau = [];
        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $tableau[] = (new UtilisateurRepository)->construireDepuisTableauSQL($utilisateurFormatTableau);
        }

        return $tableau;
    }


    protected function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]),
            $trajetTableau["prix"],
            (new UtilisateurRepository())->recupererParClePrimaire($trajetTableau["conducteurLogin"]),
            $trajetTableau["nonFumeur"]
        );

        $trajet->setPassagers(self::recupererPassagers($trajet));
        return $trajet;
    }

    protected function getNomTable(): string
    {
        return 'trajet';
    }

    protected function getNomClePrimaire() : string {
        return 'id';
    }

    protected function getNomsColonnes(): array
    {
        return ["id", "depart", "arrivee", "date", "prix", "conducteurLogin", "nonFumeur"];
    }

    protected function formatTableauSQL(Trajet $trajet): array
    {
        return array(
            "idTag" => $trajet->getId(),
            "departTag" => $trajet->getDepart(),
            "arriveeTag" => $trajet->getArrivee(),
            "dateTag" => $trajet->getDate()->format('Y-m-d H:i:s'),
            "prixTag" => $trajet->getPrix(),
            "conducteurLoginTag" => $trajet->getConducteur()->getLogin(),
            "nonFumeurTag" => $trajet->estNonFumeur()
        );
    }
}