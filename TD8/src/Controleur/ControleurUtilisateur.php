<?php


namespace App\Covoiturage\Controleur;
use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Lib\VerificationEmail;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use Exception;

class ControleurUtilisateur extends ControleurGenerique
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        /*self::deposerCookie();
        self::lireCookie();*/
        self::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherDetail(): void
    {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
        if (is_null($utilisateur)) {
            self::afficherErreur($_GET['login'] . " n'existe pas");
        } else {
            self::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "Detail de l'utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);  //"redirige" vers la vue

        }
    }

    public static function afficherFormulaireCreation(): void
    {
        self::afficherVue('vueGenerale.php', [ "titre" => "Formulaire de création d'un Utilisateur", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);  //"redirige" vers la vue
    }

    public static function creerDepuisFormulaire(): void
    {
        $utilisateur = self::construireDepuisFormulaire($_GET);
        if(!ConnexionUtilisateur::estAdministrateur()){
            $utilisateur->setEstAdmin(false);
        }
        (new UtilisateurRepository)->ajouter($utilisateur);
        VerificationEmail::envoiEmailValidation($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        $mdp = $_GET['mdp'];
        $mdp2 = $_GET['mdp2'];
        if ($mdp != $mdp2) {
            self::afficherErreur("Les mots de passe ne correspondent pas");
        } else {
            self::afficherVue('vueGenerale.php', ["utilisateurs" => $utilisateurs,"titre" => "Creation d'un utilisateur", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
        }
    }

    public static function afficherErreur(string $messageErreur = "") : void {
        self::afficherVue('vueGenerale.php', ["titre" => "Erreur", "messageErreur" => $messageErreur, "cheminCorpsVue" => "utilisateur/erreur.php"]);
    }

    public static function supprimer(): void
    {
        if(!isset($_GET['login']))
            self::afficherErreur("Le login n'est pas renseigné");
        else if(is_null((new UtilisateurRepository())->recupererParClePrimaire($_GET['login'])))
            self::afficherErreur("L'utilisateur n'existe pas");
        else if ($_GET['login'] != (new ConnexionUtilisateur())->getLoginUtilisateurConnecte())
            self::afficherErreur("Pas bien de hacker 'les suppressions'");
        else {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
            $login = $utilisateur->getLogin();
            if (is_null($utilisateur)) {
                self::afficherErreur($_GET['login'] . " n'existe pas");
            } else {
                (new UtilisateurRepository())->supprimerParLogin($_GET['login']);
                $utilisateurs = (new UtilisateurRepository())->recuperer();
                self::afficherVue('vueGenerale.php', ["login" => $login, "titre" => "Utilisateur supprimé", "uti" => $utilisateur, "utilisateurs" => $utilisateurs, "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php"]);
            }
        }
    }

    public static function afficherFormulaireMiseAJour()
    {

        if ($_GET['login'] !== ConnexionUtilisateur::getLoginUtilisateurConnecte() && !ConnexionUtilisateur::estAdministrateur() || (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']) == null) {
            self::afficherErreur('La mise à jour n’est possible que pour l’utilisateur connecté');
        }
        else if (!ConnexionUtilisateur::estAdministrateur() && (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']) == null) {
            self::afficherErreur("Le login n'existe pas");
        }
        else {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']);
            if (is_null($utilisateur)) {
                self::afficherErreur($_GET['login'] . " n'existe pas");
            } else {
                self::afficherVue('vueGenerale.php', ["utilisateur" => $utilisateur, "titre" => "FormulaireMiseAJour Utilisateur", "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php"]);
            }
        }

    }

    public static function mettreAJour() : void
    {
        if ($_GET['login'] !== ConnexionUtilisateur::getLoginUtilisateurConnecte() && !ConnexionUtilisateur::estAdministrateur() || !ConnexionUtilisateur::estAdministrateur() && (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']) == null) {
            self::afficherErreur('La mise à jour n’est possible que pour l’utilisateur connecté');
        }
        else if (ConnexionUtilisateur::estAdministrateur() && (new UtilisateurRepository())->recupererParClePrimaire($_GET['login']) == null) {
            self::afficherErreur("Le login n'existe pas");
        }
        else{
        if(!isset($_GET['login']) || !isset($_GET['nom']) || !isset($_GET['prenom']) || !isset($_GET['mdp']) || !isset($_GET['mdp2']) || !isset($_GET['ancienmdp']))
            self::afficherErreur("Champ(s) manquant(s)");
        else if(is_null((new UtilisateurRepository())->recupererParClePrimaire($_GET['login'])))
            self::afficherErreur("L'utilisateur n'existe pas");
        else if( $_GET['login'] != (new ConnexionUtilisateur())->getLoginUtilisateurConnecte() && !ConnexionUtilisateur::estAdministrateur())
            self::afficherErreur("Pas bien de hacker les formulaires");
        else{
            $mdp = $_GET['mdp'];
            $mdp2 = $_GET['mdp2'];
            $ancienmdp = $_GET['ancienmdp'];

            $utilisateur = self::construireDepuisFormulaire($_GET);
            $login = $utilisateur->getLogin();
            if (is_null($utilisateur)) {
                self::afficherErreur();
            } else {
                if ($mdp != $mdp2) {
                    self::afficherErreur("Les mots de passe ne correspondent pas");
                } else {
                    if(MotDePasse::verifier($ancienmdp,$utilisateur->getMdpHache())){
                        (new UtilisateurRepository)->mettreAJour($utilisateur);
                        $utilisateurs = (new UtilisateurRepository())->recuperer();
                        self::afficherVue('vueGenerale.php', ["login" => $_GET['login'],"utilisateur" => $utilisateur, "utilisateurs" => $utilisateurs,"titre" => "Utilisateur MiseAJour", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php"]);
                    }else{
                        $ancienmdp = MotDePasse::verifier($ancienmdp,$utilisateur->getMdpHache());
                        $mdpentree = $_GET['ancienmdp'];
                        self::afficherErreur("L'ancien mot de passe est incorrect entré: ".$mdpentree." - ".$ancienmdp ."attendu");
                    }
                }
            }
        }
        }

    }

    /**
     * @return Utilisateur
     */
    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $utilisateur = new Utilisateur($tableauDonneesFormulaire['login'],
            $tableauDonneesFormulaire['nom'],
            $tableauDonneesFormulaire['prenom'],
            MotDePasse::hacher($tableauDonneesFormulaire['mdp']),
            isset($tableauDonneesFormulaire['estAdmin']),
            '',
            $tableauDonneesFormulaire['email'],
            MotDePasse::genererChaineAleatoire()
        );
        return $utilisateur;
    }

    /*public static function deposerCookie()
    {
        $utilisateur = new Utilisateur("Test", "Test", "Test");
        Cookie::enregistrer("TestCookie", $utilisateur, time() + 3600);
    }


    public static function lireCookie()
    {
        echo Cookie::lire("TestCookie");
    }*/

    public static function afficherFormulairePreference()
    {
        ControleurGenerique::afficherFormulairePreference();
    }

    public static function enregistrerPreference()
    {
        ControleurGenerique::enregistrerPreference();
    }

    /*public static function testerSession(): void
    {
        try {
            // Start the session
            $session = Session::getInstance();
            echo "Session started. Session ID: " . session_id() . "<br>";

            // Write different types of variables to the session
            $session->enregistrer("utilisateur", "Cathy Penneflamme");
            $session->enregistrer("tableau", ["clé1" => "valeur1", "clé2" => "valeur2"]);
            $session->enregistrer("objet", (object)["propriété1" => "valeur1", "propriété2" => "valeur2"]);

            // Read and display the session variables
            echo "Utilisateur: " . $session->lire("utilisateur") . "<br>";
            echo "Tableau: ";
            print_r($session->lire("tableau"));
            echo "<br>Objet: ";
            print_r($session->lire("objet"));
            echo "<br>";

            // Delete a session variable
            $session->supprimer("utilisateur");
            echo "Utilisateur après suppression: " . $session->lire("utilisateur") . "<br>";

            // Destroy the session
            $session->detruire();
            echo "Session destroyed. Session ID: " . session_id() . "<br>";
        } catch (Exception $e) {
            echo "Erreur: " . $e->getMessage();
        }
    }*/

    public static function afficherFormulaireConnexion() :void
    {
        self::afficherVue('vueGenerale.php', ["titre" => "connexion", "cheminCorpsVue" => "utilisateur/formulaireConnexion.php"]);

    }

    public static function connecter() : void
    {
        if(!isset($_GET["login"]))
            self::afficherErreur("Le login n'est pas renseigné");
        if (!isset($_GET["mdp"]))
            self::afficherErreur("Le mot de passe n'est pas renseigné");

        $login = $_GET["login"];
        $mdp = $_GET["mdp"];

        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if (is_null($utilisateur)) {
            self::afficherErreur("L'utilisateur n'existe pas");
        } else {
            if (MotDePasse::verifier($mdp, $utilisateur->getMdpHache())) {
                ConnexionUtilisateur::connecter($login);
                self::afficherVue('vueGenerale.php', ["titre" => "Connexion réussie", "utilisateur" => $utilisateur,"cheminCorpsVue" => "utilisateur/utilisateurConnecte.php"]);
            } else {
                self::afficherErreur("Le mot de passe est incorrect");
            }
        }
    }

    public static function deconnecter() : void
    {
        ConnexionUtilisateur::deconnecter();
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        self::afficherVue('vueGenerale.php', ["titre" => "Connexion réussie", "utilisateurs" => $utilisateurs, "cheminCorpsVue" => "utilisateur/utilisateurDeconnecte.php"]);

    }

    public static function validerEmail() : void{
        if(isset($_GET['login']))
            $login = $_GET['login'];
        else
            self::afficherErreur("Le login n'est pas renseigné");
        if(isset($_GET['nonce']))
            $nonce = $_GET['nonce'];
        else
            self::afficherErreur("Le nonce n'est pas renseigné");

        VerificationEmail::traiterEmailValidation($login, $nonce);

    }

}

?>