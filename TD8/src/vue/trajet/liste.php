<?php
/** @var Trajet[] $trajets */
echo "<h1>Liste des Trajets</h1>";
foreach ($trajets as $trajet) {
    $loginHTML = htmlspecialchars($trajet->getId());
    $loginURL = rawurlencode($trajet->getId());
    echo $trajet->__toString() . " <a href='controleurFrontal.php?controleur=trajet&action=afficherDetail&id=" . $loginURL . "'><button>Voir détail</button></a> <a href='controleurFrontal.php?controleur=trajet&action=afficherFormulaireMiseAJour&id=" . $loginURL ."'><button>MettreAJour</button></a>  <a href='controleurFrontal.php?controleur=trajet&action=supprimer&id=" . $loginURL . "'><button>Supprimer</button></a></p>";
}
echo "<a href='controleurFrontal.php?controleur=trajet&action=afficherFormulaireCreation'><button>Créer un trajet</button></a>";
?>