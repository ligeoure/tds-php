<!DOCTYPE html>
<?php
/** @var ModeleUtilisateur[] $utilisateur */

use App\Covoiturage\Lib\ConnexionUtilisateur;

$loginHTML = htmlspecialchars($utilisateur->getLogin());
$nomHTML = htmlspecialchars($utilisateur->getNom());
$prenomHTML = htmlspecialchars($utilisateur->getPrenom());
?>
<html>
<head>
    <meta charset="utf-8" />
    <title></title>
</head>

<body>

<form method="get" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" value="<?= $loginHTML?>" type="text" placeholder="leblancj" name="login" id="login_id" readonly/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom&#42;</label>
            <input class="InputAddOn-field" value="<?= $nomHTML?>" type="text" placeholder="LeBlanc" name="nom" id="nom_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prenom&#42;</label>
            <input class="InputAddOn-field" value="<?= $prenomHTML?>" type="text" placeholder="Juste" name="prenom" id="prenom_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp2_id">Ancien mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="ancienmdp" id="ancienmdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp2_id">Vérification du mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id" required>
        </p>
        <?php if(ConnexionUtilisateur::estAdministrateur()){?>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="estAdmin_id">Administrateur</label>
            <input class="InputAddOn-field" type="checkbox" placeholder="" name="estAdmin" id="estAdmin_id" <?php echo ($utilisateur->getEstAdmin()) ? 'checked' : ''; ?>>
        </p>
        <?php } ?>
        <p class="InputAddOn">
            <input class="InputAddOn-field" type='hidden' name='action' value='mettreAJour'>
            <input class="InputAddOn-field" type='hidden' name='controleur' value='utilisateur'>
        </p>
        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>

</body>
</html>