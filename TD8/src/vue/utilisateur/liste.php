<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
<?php
/** @var ModeleUtilisateur[] $utilisateurs */

use App\Covoiturage\Lib\ConnexionUtilisateur;

foreach ($utilisateurs as $utilisateur) {
    $loginURL = rawurlencode($utilisateur->getLogin());
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    echo '<p> <b>Utilisateur de login : </b>' . $loginHTML . '  <a href="controleurFrontal.php?action=afficherDetail&login=' . $loginURL . '"><button>Voir détail</button></a>';
    if (ConnexionUtilisateur::estAdministrateur()){echo '<a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=' . rawurlencode($loginURL) . '"><button>Mettre à jour</button></a></p>';}}
?>
</body>
</html>
