<?php

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Modele\Utilisateur;
/** @var Utilisateur[] $utilisateur */

$loginCo = ConnexionUtilisateur::getLoginUtilisateurConnecte();

$result = "Nom: " . htmlspecialchars($utilisateur->getNom()) . " Prenom:" . htmlspecialchars($utilisateur->getPrenom()) . " Login:" . htmlspecialchars($utilisateur->getLogin()) . " ";
if ($loginCo == $utilisateur->getLogin()) {
    $loginURL = htmlspecialchars($utilisateur->getLogin());
    $result .= '<a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=' . rawurlencode($loginURL) . '"><button>Mettre à jour</button></a> <a href="controleurFrontal.php?action=supprimer&login=' . rawurlencode($loginURL) . '"><button>Supprimer</button></a>';
}
echo $result;

?>