<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="../ressources/navstyle.css">
    <meta charset="UTF-8">
    <title><?php
        /**
         * @var string $titre
         */

        use App\Covoiturage\Lib\ConnexionUtilisateur;
        use App\Covoiturage\Lib\MotDePasse;

        echo $titre; ?></title>
</head>
<body>
<header>
    <nav>
        <ul>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
            </li><li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
            </li><li>
                <a href="controleurFrontal.php?action=afficherFormulairePreference"><img src="../ressources/img/heart.png"></a>
            </li>
            <?php if(ConnexionUtilisateur::estConnecte() && ConnexionUtilisateur::estAdministrateur()) {?>
            <li>
                <a href="controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur"><img src="../ressources/img/add-user.png"></a>
            </li>
            <?php } ?>
            <?php if(!ConnexionUtilisateur::estConnecte()) {?>
            <li>
                <a href="controleurFrontal.php?action=afficherFormulaireConnexion&controleur=utilisateur"><img src="../ressources/img/enter.png"></a>
            </li> <?php } else { ?>
                <li>
                    <a <?= $login = ConnexionUtilisateur::getLoginUtilisateurConnecte() ?> href="controleurFrontal.php?action=afficherDetail&controleur=utilisateur&login=<?php echo rawurlencode($login) ?>" ><img src="../ressources/img/user.png"></a>
                </li><li>
                    <a href="controleurFrontal.php?action=deconnecter&controleur=utilisateur"><img src="../ressources/img/logout.png"></a>
                </li>
            <?php }?>
        </ul>
    </nav>


</header>
<main>
    <?php
    /**
     * @var string $cheminCorpsVue
     */
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage de ...
    </p>
</footer>
</body>
</html>

