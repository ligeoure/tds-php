<?php
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

use App\Covoiturage\Controleur\ControleurGenerique;
use App\Covoiturage\Controleur\ControleurUtilisateur;
use App\Covoiturage\Lib\PreferenceControleur;
use App\Covoiturage\Modele\HTTP\Cookie;

/*unset($_COOKIE['preferenceControleur']);
setcookie('preferenceControleur', '', 1);*/

$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

if (PreferenceControleur::lire()) {
    $preferenceControleur = PreferenceControleur::lire();
}

$controleur = $_GET['controleur'] ?? $preferenceControleur ?? 'utilisateur';
$nomDeClasseControleur = "App\Covoiturage\Controleur\Controleur" . ucfirst($controleur);

if (!class_exists($nomDeClasseControleur)) {
    ControleurUtilisateur::afficherErreur("La classe $nomDeClasseControleur n'existe pas");
    return;
}

$action = $_GET['action'] ?? 'afficherListe';

/*if ($action === 'testerSession') {
    ControleurUtilisateur::testerSession();
    return;
}*/

if (!in_array($action, get_class_methods($nomDeClasseControleur))) {
    ControleurGenerique::afficherErreur("Action introuvable : " . $_GET["action"]);
} else {
    $nomDeClasseControleur::$action();
}